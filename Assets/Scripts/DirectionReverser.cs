﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionReverser : MonoBehaviour
{
    Rigidbody rigidbody;

    public bool inverseX, inverseY, inverseZ;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        UpdateDirection();
    }

    private void UpdateDirection()
    {
        if (inverseX)
        {
            rigidbody.useGravity = false;
            //GetComponent<ConstantForce>().force = new Vector3(-10, 0, 0);
            rigidbody.AddForce(-Physics.gravity.x, Physics.gravity.y, Physics.gravity.z);
        }

        if (inverseY)
        {
            rigidbody.useGravity = false;
            rigidbody.AddForce(Physics.gravity.x, -Physics.gravity.y, Physics.gravity.z);
            //GetComponent<ConstantForce>().force = new Vector3(0, -10, 0);
        }

        if (inverseZ)
        {
            rigidbody.useGravity = false;
            rigidbody.AddForce(Physics.gravity.x, Physics.gravity.y, -Physics.gravity.z);
            //GetComponent<ConstantForce>().force = new Vector3(0, -10, 0);
        }
    }
}
