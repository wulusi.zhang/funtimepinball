﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Vector3 m_targetPosition;
    private Vector3 m_startPos;

    private Coroutine m_movingPosCoroutine;
    private bool m_travellingToTarget;

    public float m_movementTime;
    public AnimationCurve m_animCurve;

    [Header("BoxCast")]
    public Vector3 m_boxcastSize;
    public Vector3 m_boxcastOffset;
    public LayerMask m_boxcastMask;

    [Header("Debugging")]
    public bool m_debugging;
    public Color m_debugColor = Color.red, m_debugColor2 = Color.cyan;

    private void Start()
    {
        m_startPos = transform.position;
    }

    public void TravelPosChanged()
    {
        m_travellingToTarget = !m_travellingToTarget;
        if (m_movingPosCoroutine == null)
        {
            m_movingPosCoroutine = StartCoroutine(MoveMe());
        }

    }

    private IEnumerator MoveMe()
    {
        float timer = (m_travellingToTarget)? 0 : m_movementTime;
        bool isMoving = true;
        Vector3 lastPos;
        while (isMoving)
        {
            timer += ((m_travellingToTarget) ? 1 : -1) * Time.deltaTime;
            lastPos = transform.position;
            transform.position = Vector3.Lerp(m_startPos, m_startPos+m_targetPosition, m_animCurve.Evaluate(timer / m_movementTime));
            MoveEverythingOnMe(transform.position - lastPos);

            yield return null;
            if(m_travellingToTarget && timer >= m_movementTime ||
                !m_travellingToTarget && timer <=0)
            {
                isMoving = false;
            }
        }
        m_movingPosCoroutine = null;
    }

    private void MoveEverythingOnMe(Vector3 p_movementVector)
    {
        Collider[] cols = Physics.OverlapBox(transform.position + m_boxcastOffset, m_boxcastSize/2, transform.rotation, m_boxcastMask);
        foreach (Collider col in cols)
        {
            col.transform.position += p_movementVector;
        }

    }

    private void OnDrawGizmos()
    {
        if (!m_debugging) return;
        Gizmos.color = m_debugColor;
        Gizmos.DrawWireSphere(transform.position+m_targetPosition, 0.25f);

        Gizmos.color = m_debugColor2;
        Gizmos.DrawWireCube(transform.position + m_boxcastOffset, m_boxcastSize);
    }
}
