﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BallControlZoneEvent : UnityEngine.Events.UnityEvent { }
public class BallControlZone : MonoBehaviour
{
    public BallControlZoneEvent m_ballControlZoneToggleEvent;
    private void OnTriggerEnter(Collider other)
    {
        m_ballControlZoneToggleEvent.Invoke();
    }
    private void OnTriggerExit(Collider other)
    {
        m_ballControlZoneToggleEvent.Invoke();
    }
}
