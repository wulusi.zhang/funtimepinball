﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    public float m_gravityForce;
    public bool isReverseX, isReverseY, isReverseZ;
    private Rigidbody m_rb;
    private void Awake()
    {
        m_rb = GetComponent<Rigidbody>();
    }
    private void FixedUpdate()
    {
        if (isReverseX)
        {
            m_rb.useGravity = false;
            m_rb.AddForce(new Vector3(-1, 1, 1) * m_gravityForce, ForceMode.Acceleration);
        }

        if (isReverseY)
        {
            m_rb.useGravity = false;
            m_rb.AddForce(new Vector3(1, -1, 1) * m_gravityForce, ForceMode.Acceleration);
        }

        if (isReverseZ)
        {
            m_rb.useGravity = false;
            m_rb.AddForce(new Vector3(1, 1, -1) * m_gravityForce, ForceMode.Acceleration);
        }
    }

    private void GravityChange()
    {
        m_rb.AddForce(Vector3.down * m_gravityForce, ForceMode.Acceleration);
    }
}
