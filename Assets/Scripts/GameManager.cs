﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public void ChangeScene()
    {
        //Debug.Log("Button pressed");

        var SceneToLoad = (SceneManager.GetActiveScene().buildIndex + 1) % (SceneManager.sceneCountInBuildSettings);


        //Debug.Log("Current count " + SceneManager.sceneCountInBuildSettings);
        //Debug.Log("Current scene is " + SceneManager.GetActiveScene().buildIndex);
        //Debug.Log("Next Scene Index is " + SceneToLoad);

        SceneManager.LoadScene(SceneToLoad);
    }

    public void ChangeScene(int p_sceneIndex)
    {
        SceneManager.LoadScene(p_sceneIndex);
    }

    public void ReloadScene()
    {
        Invoke("ActuallyReloadScene", 1f);    
    }

    public void ActuallyReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
