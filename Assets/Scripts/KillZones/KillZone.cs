﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KillZoneEvent : UnityEngine.Events.UnityEvent { }
public class KillZone : MonoBehaviour
{
    public KillZoneEvent m_killPlayer;

    private void OnTriggerEnter(Collider other)
    {
        m_killPlayer.Invoke();
        other.GetComponent<KillBall>().RespawnMe();
    }
}
