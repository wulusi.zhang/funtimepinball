﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float rotationSpeed = 5;
    Rigidbody myRB;

    private void Awake()
    {
        myRB = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        myRB.angularVelocity = Vector3.up * rotationSpeed;
    }
}
