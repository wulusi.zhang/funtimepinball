﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGyro : MonoBehaviour
{
    public bool isManualGyro;

    //[Header("Tweaks")]
    //[SerializeField]
    //private Quaternion baseRotation = new Quaternion(1, 1, 1, 0);
    // Start is called before the first frame update
    void Start()
    {
        GyroManager._gyroManager.EnableGyro();
    }

    // Update is called once per frame
    void Update()
    {
        followGyro();
    }

    public void followGyro()
    {

        transform.localRotation = GyroManager._gyroManager.GetGyroRotation();
    }
}
