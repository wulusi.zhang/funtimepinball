﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallZone : MonoBehaviour
{
    public GameObject m_targetBall;

    public bool m_complete;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == m_targetBall)
        {
            m_complete = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == m_targetBall)
        {
            m_complete = false;
        }
    }
}
