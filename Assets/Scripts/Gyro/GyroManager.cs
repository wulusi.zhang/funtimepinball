﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroManager : MonoBehaviour
{

    #region Singleton
    private static GyroManager gyroManager;

    public bool isFlat;

    public static GyroManager _gyroManager
    {
        get
        {
            if (gyroManager == null)
            {
                gyroManager = FindObjectOfType<GyroManager>();
                if (gyroManager == null)
                {
                    gyroManager = new GameObject("GyroManager", typeof(GyroManager)).GetComponent<GyroManager>();
                }
            }

            return gyroManager;
        }

        set
        {
            gyroManager = value;
        }
    }
    #endregion

    private Gyroscope gyro;
    private Quaternion rotation;
    private bool gyroActive;

    public void EnableGyro()
    {
        if (gyroActive)
        {
            return;
        }

        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            gyroActive = gyro.enabled;
        }
        else
        {
            Debug.Log("Gyro Inactive");
        }

    }

    // Update is called once per frame
    void Update()
    {
        DebugGyro(rotation);
    }

    public void DebugGyro(Quaternion _rotation)
    {
        if (gyroActive)
        {
            rotation = gyro.attitude;
            Input.gyro.updateInterval = 0.0167f;
            Debug.Log("current rotation is:" + rotation);
        }
        else
        {
            rotation = Quaternion.Euler(new Vector3(Input.GetAxis("Horizontal") * 15f, Input.GetAxis("Vertical") * 15f, 0));
            Debug.Log("Gyro Inactive" + rotation);
        }
    }

    public Quaternion GetGyroRotation()
    {
        return new Quaternion(0.5f, 0.5f, -0.5f, 0.5f) * rotation * new Quaternion(0, 0, 1, 0);
    }
}
