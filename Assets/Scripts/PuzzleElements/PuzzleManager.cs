﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PuzzleManagerEvent : UnityEngine.Events.UnityEvent { }
public class PuzzleManager : MonoBehaviour
{
    public List<BallZone> m_ballZones;
    public PuzzleManagerEvent m_puzzleComplete;

    private bool m_complete;

    // Update is called once per frame
    void Update()
    {
        if (!m_complete)
        {
            CheckCompletion();
        }
    }
    private void CheckCompletion()
    {
        bool completed = true;
        foreach(BallZone zone in m_ballZones)
        {
            if (!zone.m_complete)
            {
                completed = false;
            }
        }

        if (completed)
        {
            m_complete = true;
            m_puzzleComplete.Invoke();
        }
    }
}
