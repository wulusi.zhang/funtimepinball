﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerInputEvent : UnityEngine.Events.UnityEvent { }

public class PlayerInput : MonoBehaviour
{
    public List<Accelerometer> m_ballControllers;

    public bool m_phoneControls;

    public PlayerInputEvents m_playerEvents;

    [System.Serializable]
    public struct PlayerInputEvents
    {
        public PlayerInputEvent m_playerTapped;
    }

    private bool m_screenTapped;

    private void Update()
    {
        Vector3 newInput = Vector3.zero;
        Quaternion newRotation = Quaternion.Euler(Vector3.zero);

        if (m_phoneControls)
        {
            newInput = Input.acceleration;
            if (!m_screenTapped)
            {
                if (Input.touchCount > 0)
                {
                    m_playerEvents.m_playerTapped.Invoke();
                    m_screenTapped = true;
                }
            }
            else
            {
                if (Input.touchCount <= 0)
                {
                    m_screenTapped = false;
                }
            }
        }
        else
        {
            newInput = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);

            if (Input.GetMouseButtonDown(0))
            {
                m_playerEvents.m_playerTapped.Invoke();
            }

            foreach (Accelerometer ball in m_ballControllers)
            {
                ball.DebugBall(newInput);
            }
        }
    }
}
