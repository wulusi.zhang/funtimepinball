﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerometer : MonoBehaviour
{
    public
    bool isFlat = true;
    public
    bool isReverse;

    public float forceConstant;
    private Rigidbody _rb;

    Vector3 tilt;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame

    public void DebugBall(Vector3 p_input)
    {
        if (!isReverse)
        {
            tilt = p_input;
        }
        else
        {
            tilt = new Vector3(-p_input.x, -p_input.y, -p_input.z);
        }

        if (isFlat)
        {
            tilt = Quaternion.Euler(90, 0, 0) * tilt;
        }

        _rb.AddForce(tilt * forceConstant);

        Debug.DrawRay(transform.position + Vector3.up, tilt, Color.cyan);
    }
}
